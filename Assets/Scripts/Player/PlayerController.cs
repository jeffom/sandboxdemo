using Cinemachine;
using System;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour
{    
    private float speed = 10f;   
    private float camSpeed = 2f;
    
    CustomInputController m_inputControls;
    CharacterController m_characterController;

    public Transform CameraTransform { get; set; }
    public CinemachineFreeLook CinemachineCamera { get; set; }

    float m_turnSmoothTime = 0.1f;
    float m_turnVelocity;

    void Awake()
    {
        m_characterController = GetComponent<CharacterController>();
        m_inputControls = new CustomInputController();
    }

    void OnEnable()
    {
        m_inputControls.Enable();
    }

    private void Update()
    {
        if (CameraTransform == null) return;

        Vector2 moveInput = m_inputControls.Player.Move.ReadValue<Vector2>();
        Vector3 moveDirection = new Vector3(moveInput.x, 0, moveInput.y).normalized;

        if (moveDirection.magnitude > 0.1f)
        {                       
            float targetAngle = Mathf.Atan2(moveDirection.x, moveDirection.z) * Mathf.Rad2Deg + CameraTransform.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref m_turnVelocity, m_turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            Vector3 charDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            m_characterController.Move(charDir.normalized * Time.deltaTime * speed);
        }

        Vector2 lookInput = m_inputControls.Player.Look.ReadValue<Vector2>();
        if (lookInput.magnitude > 0.1f)
        {
            CinemachineCamera.m_XAxis.Value -= lookInput.x * camSpeed;            
        }

    }

    void OnDisable()
    {
        m_inputControls.Disable();
    }        
}
