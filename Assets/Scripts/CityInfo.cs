﻿using System;
using System.Collections.Generic;

[System.Serializable]
public class CityInfo
{
    public string name;
    public string id;
    public string country;
    public Weather weather;

    public enum DayTime
    {
        Morning,
        Noon,
        Evening,
        Night
    }

    Dictionary<string, TimeSpan> m_timeZoneMap;

    CityInfo()
    {
        CreateTimeZoneMap();
    }

    void CreateTimeZoneMap()
    {
        var timeZones = TimeZoneInfo.GetSystemTimeZones();
        m_timeZoneMap =  new Dictionary<string, TimeSpan>();

        //Mapped from https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
        m_timeZoneMap.Add("London", new TimeSpan());
        m_timeZoneMap.Add("Hong Kong", new TimeSpan(8, 0, 0));
        m_timeZoneMap.Add("Berlin", new TimeSpan(1, 0, 0));
        m_timeZoneMap.Add("Moscow", new TimeSpan(3, 0, 0));
        m_timeZoneMap.Add("Sydney", new TimeSpan(10, 0, 0));
        m_timeZoneMap.Add("New York", new TimeSpan(-5, 0, 0));
    }

    public DateTime TimeStampToDate()
    {
        DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        dateTime = dateTime.AddSeconds(weather.timestamp);
        var zoneTimeSpan = m_timeZoneMap[name];
        return dateTime.Add(zoneTimeSpan);
    }

    public DayTime TimestampToEnum()
    {
        DayTime dayTime = DayTime.Morning;
        DateTime currentDate = TimeStampToDate();

        if (currentDate.Hour >= 5 && currentDate.Hour < 12)
        {
            dayTime = DayTime.Morning;
        }
        else if (currentDate.Hour >= 12 && currentDate.Hour < 14)
        {
            dayTime = DayTime.Noon;
        }
        else if(currentDate.Hour >= 14 && currentDate.Hour < 18)
        {
            dayTime = DayTime.Evening;
        }
        else if(currentDate.Hour >= 18 || currentDate.Hour < 5)
        {
            dayTime = DayTime.Night;
        }

        return dayTime;
    }
}

[System.Serializable]
public class Weather
{
    public Temperature temperature;
    public long timestamp;
}

[System.Serializable]
public class Temperature
{
    public float actual;
}

