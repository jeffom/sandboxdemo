﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CreatorModeController : MonoBehaviour
{
    [SerializeField]
    GameObject m_gameContainer;

    [SerializeField]
    CreateModeConfig m_createConfig;

    [SerializeField]
    CreateMenu m_createMenu;

    [SerializeField]
    CityInfoController m_cityInfoController;

    [SerializeField]
    Camera m_modeCamera;

    GameObject m_toPlacePrefabReference;
    GameObject m_toPlaceObject;
    bool m_isMovingCamera;

    List<GameObject> m_createdObjects = new List<GameObject>();
    CustomInputController m_inputControls;
    Action m_onBackCallback;
    Coroutine m_zoomCoroutine;

    public event Action<GameObject> OnPlayerObjectCreated;


    void Awake()
    {
        m_inputControls = new CustomInputController();
    }

    // Start is called before the first frame update
    void Start()
    {
        m_createMenu.ObjectToPlaceSelected += OnGameObjectSelected;
        m_createMenu.ObjectPlacementFinished += OnGameObjectPlacementFinished;
        m_createMenu.OnBackButtonPressed += OnBackButtonPressed;
        m_createMenu.OnFetchCityInfoPressed += FetchCityInfo;
        m_createMenu.OnMoveCameraPressed += OnMoveCameraToggled;

        m_inputControls.CreateMode.TouchDelta.performed += ctx => OnTouchDeltaUpdated(ctx);

        m_inputControls.CameraInput.SecondaryTouch.started += ctx => CameraSecondaryTouchStarted(ctx);
        m_inputControls.CameraInput.SecondaryTouch.canceled += ctx => CameraSecondaryTouchEnded(ctx);

        m_createMenu.ScaleChanged += UpdateScale;
        m_createMenu.RotationChanged += UpdateRotation;

        m_cityInfoController.OnGetInfoComplete += OnCityInfoDataFetched;
    }
    private void CameraSecondaryTouchStarted(UnityEngine.InputSystem.InputAction.CallbackContext ctx)
    {
        if (!m_isMovingCamera) return;

        m_zoomCoroutine = StartCoroutine(HandleMultipleFingersInput());
    }

    private void CameraSecondaryTouchEnded(UnityEngine.InputSystem.InputAction.CallbackContext ctx)
    {
        if (m_zoomCoroutine != null)
        {
            StopCoroutine(m_zoomCoroutine);
            m_zoomCoroutine = null;
        }
    }

    IEnumerator HandleMultipleFingersInput()
    {
        const float CAMERA_ZOOM_SPEED = 15f;
        const float CAMERA_MOVE_SPEED = 10f;

        float previousDistance = 0f, distance = 0f;
        Vector2 primaryDelta = Vector2.zero, secondaryDelta = Vector2.zero, previousPrimary = Vector2.zero, previousSecondary = Vector2.zero;

        while (true)
        {
            Vector2 currentPrimary = m_inputControls.CameraInput.PrimaryFingerPosition.ReadValue<Vector2>();
            Vector2 currentSecondary = m_inputControls.CameraInput.SecondaryFingerPosition.ReadValue<Vector2>();
            distance = Vector2.Distance(currentPrimary, currentSecondary);

            primaryDelta = currentPrimary - previousPrimary;
            secondaryDelta = currentSecondary - previousSecondary;

            //is pinching?
            if (Vector2.Dot(primaryDelta, secondaryDelta) < -0.9f)
            {
                if (distance > previousDistance)
                {
                    Vector3 toPosition = m_modeCamera.transform.position -  m_modeCamera.transform.forward;
                    m_modeCamera.transform.position = Vector3.Slerp(m_modeCamera.transform.position, toPosition, Time.deltaTime * CAMERA_ZOOM_SPEED);
                }
                else if (distance < previousDistance)
                {
                    Vector3 toPosition = m_modeCamera.transform.position + m_modeCamera.transform.forward;
                    m_modeCamera.transform.position = Vector3.Slerp(m_modeCamera.transform.position, toPosition, Time.deltaTime * CAMERA_ZOOM_SPEED);
                }
            }

            //are both fingers moving in the same dir?
            if (Vector2.Dot(primaryDelta, secondaryDelta) > 0.9f)
            {
                Vector2 moveDirection = primaryDelta.normalized * -1;
                Vector3 toPosition = m_modeCamera.transform.position + new Vector3(moveDirection.x, 0f, moveDirection.y);
                m_modeCamera.transform.position = Vector3.Slerp(m_modeCamera.transform.position, toPosition, Time.deltaTime * CAMERA_MOVE_SPEED);
            }

            previousPrimary = currentPrimary;
            previousSecondary = currentSecondary;
            previousDistance = distance;

            yield return null;
        }
    }

    private void OnMoveCameraToggled(bool toggled)
    {
        m_isMovingCamera = toggled;
        
        //reset placement
        if (m_toPlaceObject != null)
        {
            Destroy(m_toPlaceObject);
        }

        m_createMenu.ConfigContainer.SetActive(false);
        m_createMenu.SelectionContainer.SetActive(!toggled);
        
    }

    private void FetchCityInfo(string cityName)
    {
        m_cityInfoController.GetWeather(cityName);
    }

    private void OnCityInfoDataFetched()
    {
        m_createMenu.SetCityInfo(m_cityInfoController.CityInfo);
        m_cityInfoController.UpdateSkyBox();
    }

    void OnEnable()
    {
        m_inputControls.Enable();
    }

    void OnDisable()
    {
        m_inputControls.Disable();
    }

    public void ShowCreateMenu()
    {
        //Initialize Menu with config
        m_createMenu.InitializeMenu(m_createConfig.PlayerPrefabs, m_createConfig.PropPrefabs);
        m_createMenu.gameObject.SetActive(true);
    }

    public void HideCreateMenu()
    {
        m_createMenu.gameObject.SetActive(false);
    }

    private void OnTouchDeltaUpdated(UnityEngine.InputSystem.InputAction.CallbackContext ctx)
    {
        if (m_toPlaceObject == null) return;

        Vector3 delta = ctx.ReadValue<Vector2>();

        //transform screen delta to world delta        
        Vector3 initialInputScreenPos = m_modeCamera.WorldToScreenPoint(m_toPlaceObject.transform.position);
        Vector3 finalInputScreenPos = initialInputScreenPos + delta;
        
        //ray cast points to get a more accurate world pos
        Ray initialScreenPosRay = m_modeCamera.ScreenPointToRay(initialInputScreenPos);
        Ray finalScreenPosRay = m_modeCamera.ScreenPointToRay(finalInputScreenPos);
        RaycastHit initialHitInfo;
        RaycastHit finalHitInfo;

        if (Physics.Raycast(initialScreenPosRay, out initialHitInfo, m_modeCamera.farClipPlane, LayerMask.GetMask("Terrain")) && 
            Physics.Raycast(finalScreenPosRay, out finalHitInfo, m_modeCamera.farClipPlane,LayerMask.GetMask("Terrain")))
        {
            Vector3 initHitWorldPos = initialHitInfo.point;
            Vector3 finalHitWorldPos = finalHitInfo.point;

            Debug.Log($"Init:{initHitWorldPos} Obj:{initialHitInfo.collider.gameObject.name}| Final:{finalHitWorldPos}");

            Vector3 deltaPos = finalHitWorldPos - initHitWorldPos;            
            m_toPlaceObject.transform.position += deltaPos;
        }
    }

    private void UpdateRotation(float delta)
    {
        if (m_toPlaceObject == null) return;

        m_toPlaceObject.transform.eulerAngles += new Vector3(0, delta, 0);
    }

    private void UpdateScale(float delta)
    {
        if (m_toPlaceObject == null) return;

        m_toPlaceObject.transform.localScale += Vector3.one * delta;

        //no pivot
        if (m_toPlaceObject.transform.parent == null)
        {
            Vector3 basePos = m_toPlaceObject.transform.position;
            basePos.y = m_toPlaceObject.transform.localScale.y * m_toPlacePrefabReference.transform.position.y;
            m_toPlaceObject.transform.position = basePos;
        }
    }

    private void OnGameObjectSelected(GameObject prefab)
    {
        m_createMenu.SelectionContainer.SetActive(false);
        m_createMenu.ConfigContainer.SetActive(true);

        var ray = m_modeCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 1f));
        RaycastHit hitInfo;
        Physics.Raycast(ray, out hitInfo);

        //camera center
        Vector3 centerPos = m_modeCamera.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, (hitInfo.point - m_modeCamera.transform.position).magnitude));
        centerPos.y = prefab.transform.position.y;
        m_toPlaceObject = GameObject.Instantiate(prefab, centerPos, Quaternion.identity);
        m_toPlacePrefabReference = prefab;
    }

    private void OnGameObjectPlacementFinished()
    {
        m_createMenu.SelectionContainer.SetActive(true);
        m_createMenu.ConfigContainer.SetActive(false);

        //we want to make sure that only one player object is placed
        if (m_toPlaceObject.tag == "Player")
        {
            //has another player object?
            var playerObject = m_createdObjects.FirstOrDefault(x => x.gameObject.tag == "Player");
            if (playerObject != null)
            {
                m_createdObjects.Remove(playerObject);
                Destroy(playerObject);
            }
        }

        m_createdObjects.Add(m_toPlaceObject);

        if (m_toPlaceObject.tag == "Player")
        {
            OnPlayerObjectCreated(m_toPlaceObject);
        }

        m_toPlaceObject = null;
    }

    public void SetBackButtonCallback(Action callback)
    {
        m_onBackCallback = callback;
    }

    private void OnBackButtonPressed()
    {
        if (m_toPlaceObject != null)
        {
            Destroy(m_toPlaceObject);
            m_toPlaceObject = null;
        }

        if (m_onBackCallback != null) m_onBackCallback();
    }
}