﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateMenu : MonoBehaviour
{
    const float SCALE_STEP = 0.5f;
    const float ROTATION_STEP = 5f;

    enum PrefabType
    {
        Player = 0,
        Prop
    }

    GameObject[] m_playerPrefabs;
    GameObject[] m_propPrefabs;

    [SerializeField]
    Button m_addObjectButton;

    [SerializeField]
    Button m_placeButton;

    [SerializeField]
    Button m_backButton;

    [SerializeField]
    Button m_moveCameraButton;

    [SerializeField]
    Dropdown m_typeDropdown;

    [SerializeField]
    Dropdown m_prefabDropdown;

    [SerializeField]
    Dropdown m_currentCityDropdown;

    [SerializeField]
    GameObject m_selectionContainer;

    [SerializeField]
    GameObject m_configContainer;

    [SerializeField]
    Camera m_modeCamera;

    [Header("Scaling")]
    [SerializeField]
    Button m_increaseScale;
    [SerializeField]
    Button m_decreaseScale;

    [Header("Rotating")]
    [SerializeField]
    Button m_increaseRotation;
    [SerializeField]
    Button m_decreaseRotation;

    [Header("City Info")]
    [SerializeField]
    Text m_labelCityName;

    [SerializeField]
    Text m_labelDate;

    [SerializeField]
    Text m_labelTemperature;

    [SerializeField]
    Button m_butonFetchCityInfo;

    // Menu Events
    public event Action<float> RotationChanged;
    public event Action<float> ScaleChanged;
    public event Action ObjectPlacementFinished;
    public event Action<GameObject> ObjectToPlaceSelected;
    public event Action OnBackButtonPressed;
    public event Action<string> OnFetchCityInfoPressed;
    public event Action<bool> OnMoveCameraPressed;

    Button[] m_buttons;
    PrefabType m_selectedType;
    bool m_moveCameraToggled = false;

    public GameObject ConfigContainer => m_configContainer;
    public GameObject SelectionContainer => m_selectionContainer;

    private void Awake()
    {
        m_buttons = new Button[] { m_increaseScale, m_decreaseScale, m_increaseRotation, m_decreaseRotation };

        //TODO: Replace it with finger actions
        m_increaseRotation.onClick.AddListener(OnClickIncreaseRotation);
        m_decreaseRotation.onClick.AddListener(OnClickDecreaseRotation);

        m_increaseScale.onClick.AddListener(OnClickIncreaseScale);
        m_decreaseScale.onClick.AddListener(OnClickDecreaseScale);

        m_addObjectButton.onClick.AddListener(OnClickAddObject);
        m_placeButton.onClick.AddListener(OnClickPlaceObject);
        m_backButton.onClick.AddListener(OnClickBackButton);
        m_butonFetchCityInfo.onClick.AddListener(OnClickFetchCityInfo);
        m_moveCameraButton.onClick.AddListener(OnClickMoveCamera);

        m_typeDropdown.onValueChanged.AddListener(OnTypeDropdownChanged);
        m_prefabDropdown.onValueChanged.AddListener(OnPrefabDropdownChanged);
    }

    // Start is called before the first frame update
    void Start()
    {
        m_configContainer.SetActive(false);
    }

    private void OnClickAddObject()
    {
        var objectsArray = m_selectedType == PrefabType.Player ? m_playerPrefabs : m_propPrefabs;
        int selected = m_prefabDropdown.value;

        if (ObjectToPlaceSelected != null)
        {
            ObjectToPlaceSelected(objectsArray[selected]);
        }


        SetPlacementButtonsEnabled(true);
    }

    private void OnClickPlaceObject()
    {
        if (ObjectPlacementFinished != null)
        {
            ObjectPlacementFinished();
        }

        SetPlacementButtonsEnabled(false);
    }

    public void InitializeMenu(GameObject[] playerPrefabs, GameObject[] propPrefabs)
    {
        m_playerPrefabs = playerPrefabs;
        m_propPrefabs = propPrefabs;
        m_selectedType = (PrefabType)m_typeDropdown.value;
        UpdateOptions();
    }

    private void OnPrefabDropdownChanged(int idx)
    {

    }

    private void OnTypeDropdownChanged(int idx)
    {
        m_selectedType = (PrefabType)idx;

        UpdateOptions();
    }

    void UpdateOptions()
    {
        GameObject[] prefabs = { };

        if (m_selectedType == PrefabType.Player)
        {
            prefabs = m_playerPrefabs;
        }
        else if (m_selectedType == PrefabType.Prop)
        {
            prefabs = m_propPrefabs;
        }

        List<Dropdown.OptionData> options = new List<Dropdown.OptionData>();
        foreach (var p in prefabs)
        {
            options.Add(new Dropdown.OptionData(p.name));
        }


        m_prefabDropdown.ClearOptions();
        m_prefabDropdown.AddOptions(options);
    }

    void SetPlacementButtonsEnabled(bool enabled)
    {
        foreach (var but in m_buttons)
        {
            but.enabled = enabled;
        }
    }
    
    void OnClickIncreaseRotation()
    {
        if (RotationChanged != null) RotationChanged(ROTATION_STEP);
        Debug.Log("OnClickIncreaseRotation");
    }

    void OnClickDecreaseRotation()
    {
        if (RotationChanged != null) RotationChanged(-ROTATION_STEP);
    }

    void OnClickIncreaseScale()
    {
        if (ScaleChanged != null) ScaleChanged(SCALE_STEP);
    }

    void OnClickDecreaseScale()
    {
        if (ScaleChanged != null) ScaleChanged(-SCALE_STEP);
    }
    private void OnClickFetchCityInfo()
    {
        if (OnFetchCityInfoPressed != null) OnFetchCityInfoPressed(m_currentCityDropdown.options[m_currentCityDropdown.value].text);
    }

    private void OnClickMoveCamera()
    {
        m_moveCameraToggled = !m_moveCameraToggled;
        if (OnMoveCameraPressed != null) OnMoveCameraPressed(m_moveCameraToggled);
    }

    private void OnClickBackButton()
    {
        if (OnBackButtonPressed != null) OnBackButtonPressed();

        m_configContainer.SetActive(false);
        m_selectionContainer.SetActive(true);
    }

    public void SetCityInfo(CityInfo info)
    {
        m_labelCityName.text = $"City:{info.name} - Id:{info.id}";
        m_labelDate.text = $"Date:{info.TimeStampToDate().ToString("yyyy-MM-d HH:mm")}";
        m_labelTemperature.text = String.Format("Temperature:{0:F1}C" ,info.weather.temperature.actual - 273.15f);
    }
}
