﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Custom/Creator Config")]
public class CreateModeConfig : ScriptableObject
{
    [SerializeField]
    GameObject[] m_propPrefabs;

    [SerializeField]
    GameObject[] m_playerPrefabs;

    public GameObject[] PropPrefabs => m_propPrefabs;
    public GameObject[] PlayerPrefabs => m_playerPrefabs;
}
