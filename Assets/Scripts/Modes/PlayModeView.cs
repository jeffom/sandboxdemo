﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PlayModeView : MonoBehaviour
{
    [SerializeField]
    Button m_backButton;

    public event Action OnBackButtonPressed;

    void Start()
    {
        m_backButton.onClick.AddListener(OnClickBackButton);
    }

    private void OnClickBackButton()
    {
        if (OnBackButtonPressed != null) OnBackButtonPressed();
    }
}
