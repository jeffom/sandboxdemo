﻿using Cinemachine;
using UnityEngine;
using System;

public class PlayModeController : MonoBehaviour
{
    [SerializeField]
    CinemachineFreeLook m_cinemachineCamera;

    [SerializeField]
    GameObject m_playModeControls;

    [SerializeField]
    PlayModeView m_playModeView;

    [SerializeField]
    Camera m_modeCamera;

    GameObject m_playerObject;

    public event Action OnStopPlayMode;

    void Start()
    {
        m_playModeView.OnBackButtonPressed += OnBackButtonPressed;
    }

    private void OnBackButtonPressed()
    {
        if (OnStopPlayMode != null) OnStopPlayMode();

        m_cinemachineCamera.enabled = false;
        m_playModeControls.SetActive(false);
    }

    public GameObject PlayerObject
    {
        get
        {
            return m_playerObject;
        }
        set
        {
            m_playerObject = value;
        }
    }

    public void StartPlayMode()
    {        
        if (m_playerObject != null)
        {
            var controller = m_playerObject.GetComponent<CharacterController>();
            PlayerController playerController = controller.gameObject.GetComponent<PlayerController>();
            if (playerController == null)
            {
                playerController = controller.gameObject.AddComponent<PlayerController>();
            }

            playerController.CameraTransform = m_modeCamera.transform;
            playerController.CinemachineCamera = m_cinemachineCamera;

            m_cinemachineCamera.enabled = true;
            m_cinemachineCamera.Follow = m_playerObject.transform;
            m_cinemachineCamera.LookAt = m_playerObject.transform;
        }

        m_playModeControls.SetActive(true);
    }
}
