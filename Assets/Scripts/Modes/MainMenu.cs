﻿using System;
using UnityEngine.UI;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    Button m_createButton;

    [SerializeField]
    Button m_playButton;

    public event Action OnStartCreateMode;
    public event Action OnStartPlayMode;

    void Start()
    {
        m_createButton.onClick.AddListener(OnClickCreate);
        m_playButton.onClick.AddListener(OnClickPlay);
    }

    private void OnClickPlay()
    {
        if (OnStartPlayMode != null) OnStartPlayMode();
    }

    private void OnClickCreate()
    {
        if (OnStartCreateMode != null) OnStartCreateMode();
    }
}
