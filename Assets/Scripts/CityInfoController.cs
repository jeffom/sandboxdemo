﻿using System;
using UnityEngine;
using GraphQlClient.Core;
using UnityEngine.Networking;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Collections.Generic;

public class CityInfoController : MonoBehaviour
{
    [SerializeField] 
    GraphApi m_weatherApiReference;

    [SerializeField]
    SkyBoxData[] m_skyBoxDatas;

    CityInfo m_currentInfo;

    public CityInfo CityInfo => m_currentInfo;

    public event Action OnGetInfoComplete;

    public async void GetWeather(string cityName)
    {
        GraphApi.Query query = m_weatherApiReference.GetQueryByName("getCityByName", GraphApi.Query.Type.Query);
        query.SetArgs(new { name = cityName });
        UnityWebRequest request = await m_weatherApiReference.Post(query);        
        string data = request.downloadHandler.text;        
        JObject cityData = JObject.Parse(data);
        var cityInfo = cityData["data"]["getCityByName"].ToObject<CityInfo>();        
        m_currentInfo = cityInfo;

        if (OnGetInfoComplete != null) OnGetInfoComplete();
    }

    public void UpdateSkyBox()
    {
        var skyBoxMat = m_skyBoxDatas.FirstOrDefault(x => x.dayTime == m_currentInfo.TimestampToEnum());
        RenderSettings.skybox = skyBoxMat.material;
    }
}

[System.Serializable]
class SkyBoxData
{
    public string id;
    public CityInfo.DayTime dayTime;
    public Material material;
}
