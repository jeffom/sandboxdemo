﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class LongPressButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField]
    float m_fireInterval = 0.2f;

    float m_currentTimer = 0;
    bool m_isHolding;
    Button m_button;
    
    public void OnPointerDown(PointerEventData eventData)
    {
        m_isHolding = true;
        m_currentTimer = m_fireInterval;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        m_isHolding = false;
    }

    void Awake()
    {
        m_button = GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        if (m_isHolding)
        {
            m_currentTimer -= Time.deltaTime;
            if (m_currentTimer <= 0)
            {
                m_currentTimer = m_fireInterval;
                m_button.onClick.Invoke();
            }
        }
    }
}
