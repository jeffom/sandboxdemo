﻿using System;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    [SerializeField]
    MainMenu m_mainMenu;

    [SerializeField]
    CreatorModeController m_createModeController;

    [SerializeField]
    PlayModeController m_playModeController;

    void Start()
    {
        m_createModeController.SetBackButtonCallback(() =>
        {
            m_mainMenu.gameObject.SetActive(true);
            m_createModeController.HideCreateMenu();
        });

        m_mainMenu.OnStartCreateMode += StartCreateMode;
        m_mainMenu.OnStartPlayMode += StartPlayMode;

        m_createModeController.OnPlayerObjectCreated += OnPlayerObjectCreated;

        m_playModeController.OnStopPlayMode += OnStopPlayMode;
    }

    private void OnStopPlayMode()
    {
        m_mainMenu.gameObject.SetActive(true);
    }

    private void OnPlayerObjectCreated(GameObject obj)
    {
        m_playModeController.PlayerObject = obj;
    }

    private void StartPlayMode()
    {
        m_mainMenu.gameObject.SetActive(false);
        m_playModeController.StartPlayMode();
    }

    private void StartCreateMode()
    {
        m_mainMenu.gameObject.SetActive(false);
        m_createModeController.ShowCreateMenu();
    }
}
