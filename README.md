# Simple Sandbox project in Unity

## Instructions

Use Create to start creating objects
Set up a player object to be able to control it in play mode
Select props to enrich the scene

When you press move camera you toggle the camera movement mode
Pinch your fingers to control zoom or move two fingers at the same time to pan the camera

In create mode you can fetch some city information from cities in the dropdown,
it will reflect the current time by changing the scene skybox